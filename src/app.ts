import express from 'express';
import bodyParser from 'body-parser';
import rTracer from 'cls-rtracer';
import { dragonchainCors } from './middleware/cors';
import { notFound } from './middleware/notFound';
import { errorHandler } from './middleware/errors';
import { healthCheck } from './controller/system';
import { v1Routes } from './routes/v1';
import { Dragonchain } from './clients/dragonchain';

export class Application {
  public app: express.Application;

  public constructor() {
    this.app = express();
    this.app.use(rTracer.expressMiddleware({ useHeader: true }));

    // Express configuration
    this.app.set('port', process.env.PORT || 8080);
    this.app.set('env', process.env.NODE_ENV || 'production');
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(dragonchainCors);

    // Healthcheck endpoint should be unauthenticated and never versioned.
    this.app.get('/ping', healthCheck);

    // V1 routes
    v1Routes(this.app);

    // Handle Errors
    this.app.use(notFound);
    this.app.use(errorHandler);
  }

  public async startServer() {
    await Dragonchain.initialize();
    this.app.listen(this.app.get('port'), '0.0.0.0', () => {
      console.log('App is running at http://localhost:%d in %s mode', this.app.get('port'), this.app.get('env'));
      console.log('Press CTRL-C to stop');
    });
  }
}
