import { createClient } from 'dragonchain-sdk';
import { DragonchainClient } from 'dragonchain-sdk/dist/types/services/dragonchain-client/DragonchainClient';
import { BulkTransactionPayload } from 'dragonchain-sdk/dist/types/interfaces/DragonchainClientInterfaces';
import { checkCallError } from '../util';
import delay from 'await-delay';
import { FailureByDesign } from '../util/errors';
require('dotenv').config();

const { CHAIN_DETAILS } = process.env;
const SEARCH_DELAY = 3;
const MAX_RETRIES = 7;

export class Dragonchain {
  public static client: DragonchainClient;

  public static async initialize() {
    const { dragonchainId, endpoint, authKeyId, authKey } = JSON.parse(CHAIN_DETAILS);
    this.client = await createClient({
      dragonchainId,
      endpoint,
      authKeyId,
      authKey
    });
  }

  static async getSmartContractObject(key: string, smartContractId?: string) {
    const call = await this.client.getSmartContractObject({ key, smartContractId });
    checkCallError(call);
    try {
      call.response = JSON.parse(call.response);
    } catch (error) {}
    return call;
  }

  static async listSmartContractObjects(prefixKey: string, smartContractId?: string) {
    const call = await this.client.listSmartContractObjects({ prefixKey, smartContractId });
    checkCallError(call);
    return call.response;
  }

  static async queryTransactions(transactionType: string, redisearchQuery: string, limit?: number) {
    const call = await this.client.queryTransactions({ transactionType: transactionType, redisearchQuery: redisearchQuery, limit: limit });
    checkCallError(call);
    return call.response;
  }

  static async queryTransactionLoop(transactionType: string, redisearchQuery: string, attempts = 0) {
    if (attempts >= MAX_RETRIES) {
      throw new FailureByDesign('HEAP_ERROR', `Unable to locate smart-contract result after ${MAX_RETRIES} attempts.`);
    }
    attempts += 1; // exit criteria
    const call = await Dragonchain.queryTransactions(transactionType, redisearchQuery, 1);
    if (call.total === 1) {
      const result = call.results[0];
      return result;
    }
    await delay(1000 * SEARCH_DELAY);
    return await Dragonchain.queryTransactionLoop(transactionType, redisearchQuery, attempts);
  }

  static async createBulkTransaction(transactionList: BulkTransactionPayload[]) {
    const call = await this.client.createBulkTransaction({ transactionList });
    checkCallError(call);
    return call.response;
  }

  static async getTransaction(transactionId: string) {
    const call = await this.client.getTransaction({ transactionId });
    checkCallError(call);
    return call;
  }

  static async getBlock(blockId: string) {
    const call = await this.client.getBlock({ blockId });
    checkCallError(call);
    return call.response;
  }

  static async getVerifications(blockId: string) {
    const call = await this.client.getVerifications({ blockId });
    checkCallError(call);
    return call.response;
  }

  static async createTransaction(transactionType: string, payload: any) {
    const call = await Dragonchain.client.createTransaction({ transactionType, payload });
    checkCallError(call);
    return call.response;
  }
}

/**
 * All humans are welcome.
 */
