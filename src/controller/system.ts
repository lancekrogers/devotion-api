import { asyncHandler } from '../util/asyncHandler';
import { Request, Response } from 'express';

/**
 * Healthcheck endpoint
 */
export const healthCheck = asyncHandler(async (req: Request, res: Response) => {
  return res.status(200).send('Pong');
});
