import { Request, Response } from 'express';
import { FailureByDesign } from '../util/errors';
import { Dragonchain } from '../clients/dragonchain';
import { asyncHandler } from '../util/helpers';
import { Validator } from '../util/schemas';

const TRANSACTION_TYPE = 'devotion';
const VALIDATE = new Validator();

export const registerDevotion = asyncHandler(async (req: Request, res: Response) => {
  VALIDATE.validateRegisterDevotion(req.body);

  const payload = {
    parameters: req.body,
    method: 'register'
  };
  const post = await Dragonchain.createTransaction(TRANSACTION_TYPE, payload);
  const transactionId = post.transaction_id;
  const escapedTxnId = transactionId.replace(/-/g, '\\-');
  const invokerQuery = `@invoker:{${escapedTxnId}}`;
  const response = await Dragonchain.queryTransactionLoop(TRANSACTION_TYPE, invokerQuery);
  const responsePayload = response.payload;
  VALIDATE.validateRegisterDevotionResponse(responsePayload);

  const limitedResponse = {txnId: response.header.txn_id, payload: responsePayload}

  return res.send(limitedResponse);
});

export const cancelDevotion = asyncHandler(async (req: Request, res: Response) => {
  VALIDATE.validateCancelDevotion(req.body);

  const payload = {
    parameters: req.body,
    method: 'cancel'
  };
  const post = await Dragonchain.createTransaction(TRANSACTION_TYPE, payload);
  const transactionId = post.transaction_id;
  const escapedTxnId = transactionId.replace(/-/g, '\\-');
  const invokerQuery = `@invoker:{${escapedTxnId}}`;
  const response = await Dragonchain.queryTransactionLoop(TRANSACTION_TYPE, invokerQuery);
  const responsePayload = response.payload;
  VALIDATE.validateCancelDevotionResponse(responsePayload);

  const limitedResponse = {txnId: response.header.txn_id, payload: responsePayload }

  return res.send(limitedResponse);

});

export const getDevotions = asyncHandler(async (req: Request, res: Response) => {
  const { quipuId } = req.params;
  if (!quipuId) {
    throw new FailureByDesign('MISSING_PARAMETERS_ERROR', 'paramaters missing');
  }

  const key = `devotions/devoted/${quipuId}`;

  const { CHAIN_DETAILS } = process.env;
  const { smartContractId } = JSON.parse(CHAIN_DETAILS);
  const objects = await Dragonchain.listSmartContractObjects(key, smartContractId);
  const objectList = [];

  for (const key of objects) {
    const objectName = key.split('/')[4];
    if (objectName === 'totals') {
      continue;
    } else {
      const call = await Dragonchain.getSmartContractObject(key.slice(1), smartContractId);
      const pushObj = {};
      pushObj[objectName] = call.response;
      objectList.push(pushObj);
    }
  }
  return res.send(objectList);
});

export const getSum = asyncHandler(async (req: Request, res: Response) => {
  const { quipuId } = req.params;
  if (!quipuId) {
    throw new FailureByDesign('MISSING_PARAMETERS_ERROR', 'quipuId not included');
  }

  const key = `devotions/devoted/${quipuId}/totals`;

  const { CHAIN_DETAILS } = process.env;
  const { smartContractId } = JSON.parse(CHAIN_DETAILS);

  const call = await Dragonchain.getSmartContractObject(key, smartContractId);

  return res.send(call.response);
});
