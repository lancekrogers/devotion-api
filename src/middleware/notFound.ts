import { FailureByDesign } from '../util/errors';

export const notFound = () => {
  throw new FailureByDesign('NOT_FOUND', 'requested resource not found');
};
