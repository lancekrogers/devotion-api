import { Request, Response, NextFunction } from 'express';
import { FailureByDesign } from '../util/errors';

const getSurfaceError = (err: FailureByDesign) => {
  const surfaceError = { code: err.code, error: err.message };
  switch (err.code) {
    case 'BAD_REQUEST':
    case 'MALFORMED_INPUT':
    case 'MISSING_PARAMETERS_ERROR':
    case 'MALFORMED_JSON_ERROR':
    case 'HEAP_ERROR':
    case 'SUBSYSTEM_ERROR':
    case 'DRAGONCHAIN_CONTRACT_ERROR':
      return { httpStatusCode: 400, surfaceError };
    case 'UNAUTHORIZED':
      return { httpStatusCode: 401, surfaceError };
    case 'FORBIDDEN':
      return { httpStatusCode: 403, surfaceError };
    case 'NOT_FOUND':
      return { httpStatusCode: 404, surfaceError };
    case 'CONFLICT':
      return { httpStatusCode: 409, surfaceError };
    default:
      console.error('500 returned to client.', err);
      return {
        httpStatusCode: 500,
        surfaceError: {
          code: 'INTERNAL_SERVER_ERROR',
          error: 'An unexpected error occurred'
        }
      };
  }
};

export const errorHandler = (err: FailureByDesign, _req: Request, res: Response, next: NextFunction) => {
  if (err) {
    const { httpStatusCode, surfaceError } = getSurfaceError(err);
    return res.status(httpStatusCode).send(surfaceError);
  }
  next();
};
