import cors from 'cors';

const corsSettings = {
  origin: [/\.dragonchain\.com$/],
  methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
};

if (process.env.NODE_ENV !== 'production') {
  corsSettings.origin.push(/localhost:[0-9]{4}$/);
}

export const dragonchainCors = cors(corsSettings);
