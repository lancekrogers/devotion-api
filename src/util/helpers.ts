import { Request, Response, NextFunction } from 'express';
import { FailureByDesign } from './errors';

export const asyncHandler = (fn: any) => (req: Request, res: Response, next: NextFunction) => {
  return Promise.resolve(fn(req, res, next)).catch(next);
};

export const checkCallError = call => {
  if (!call.ok) {
    let { response } = call;
    let error = {
      type: undefined,
      details: undefined
    };
    if (response) {
      try {
        response = JSON.parse(response);
      } catch (e) {} // eslint-disable-line no-empty
      error = response.error; // eslint-disable-line prefer-destructuring
    }
    const standardErrors = [403, 404, 500];
    if (standardErrors.includes(call.status)) {
      throw new FailureByDesign(error.type, `Status: ${call.status}, ${error.details}`);
    }
    // Catch-all for non 2XX http responses
    throw new FailureByDesign('SUBSYSTEM_ERROR', `Bad response from dragonchain. Status code: ${call.status} Response: ${JSON.stringify(call.response)}`);
  }
};
