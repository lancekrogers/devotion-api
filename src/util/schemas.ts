import Ajv from 'ajv';
import { FailureByDesign } from './errors';

const registerDevotionSchema = {
  title: 'registerDevotionSchema',
  type: 'object',
  description: 'create register payload',
  properties: {
    address: { type: 'string' },
    time: { type: 'string' },
    quipuId: { type: 'string' }
  },
  required: ['address', 'time', 'quipuId']
};

const cancelDevotionSchema = {
  title: 'cancelDevotionSchema',
  type: 'object',
  description: 'cancel a registered devotion',
  properties: {
    address: { type: 'string' }
  },
  required: ['address']
};

const registerDevotionResponseSchema = {
  title: 'registerDevotionResponseSchema',
  type: 'object',
  description: 'validate dragonchain register response object',
  properties: {
    drgn:  { type: "string" },
    time: { type: "string" },
    quipuId: { type: "string" },
    fee:  { type: "string" },
    address: { type: "string" }
  },
  required: ['address', 'time', 'drgn']
};

const cancelDevotionResponseSchema = {
  title: 'cancelDevotionResponseSchema',
  type: 'object',
  description: 'validate dragonchain cancel response object',
  properties: {
    success:  { type: "string" },
  },
  required: ['success']
};

const contractErrorSchema = {
  title: 'contractErrorSchema',
  type: 'object',
  description: 'contract error schema',
  properties: {
    error: { type: 'string' }
  },
  required: ['error']
};

export class Validator {
  private ajv: any;
  private validateRegisterDevotionPayload: (payload: object) => boolean;
  private validateCancelDevotionPayload: (payload: object) => boolean;
  private validateRegisterDevotionResponsePayload: (payload: object) => boolean;
  private validateContractErrorPayload: (payload: object) => boolean;
  private validateCancelDevotionResponsePayload: (payload: object) => boolean;

  public constructor() {
    this.ajv = new Ajv({ schemaId: 'auto' });
    this.validateRegisterDevotionPayload = this.ajv.compile(registerDevotionSchema);
    this.validateCancelDevotionPayload = this.ajv.compile(cancelDevotionSchema);
    this.validateRegisterDevotionResponsePayload = this.ajv.compile(registerDevotionResponseSchema);
    this.validateContractErrorPayload = this.ajv.compile(contractErrorSchema);
    this.validateCancelDevotionResponsePayload = this.ajv.compile(cancelDevotionResponseSchema);
  }

  public validateRegisterDevotion = (payload: object) => {
    if (!this.validateRegisterDevotionPayload(payload)) {
      throw new FailureByDesign('MALFORMED_INPUT', 'invalid input for register devotion');
    }
  };

  public validateCancelDevotion = (payload: object) => {
    if (!this.validateCancelDevotionPayload(payload)) {
      throw new FailureByDesign('MALFORMED_INPUT', 'invalid input for cancel devotion');
    }
  };

  public validateRegisterDevotionResponse = (payload: object) => {
    const keys = []; 
    for (const key in payload) {
      keys.push(key)
    }
    const firstKey = keys[0];
    if (!this.validateRegisterDevotionResponsePayload(payload[firstKey])) {
      if (firstKey === 'error') {
        if (this.validateContractErrorPayload(payload)){
          throw new FailureByDesign('DRAGONCHAIN_CONTRACT_ERROR', payload['error']);
        }
      }
      throw new FailureByDesign('DRAGONCHAIN_CONTRACT_ERROR', 'Dragonchain is returning an unexpected result');
      }
  };

  private validateContractError = (payload: object) => {
    if (!this.validateContractErrorPayload(payload)) {
        throw new FailureByDesign('MALFORMED_INPUT', 'invalid input for cancel devotion');
      }
  }

  public validateCancelDevotionResponse = (payload: object) => {
    if (!this.validateCancelDevotionResponsePayload(payload)) {
      console.log(payload);
      if (this.validateContractErrorPayload(payload)){
        throw new FailureByDesign('DRAGONCHAIN_CONTRACT_ERROR', payload['error']);
      }
    throw new FailureByDesign('DRAGONCHAIN_CONTRACT_ERROR', 'Dragonchain is returning an unexpected result');
    }
  };
