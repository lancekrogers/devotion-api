type ErrorCode =
  | 'BAD_REQUEST'
  | 'UNAUTHORIZED'
  | 'FORBIDDEN'
  | 'NOT_FOUND'
  | 'CONFLICT'
  | 'SUBSYSTEM_ERROR'
  | 'HEAP_ERROR'
  | 'MALFORMED_JSON_ERROR'
  | 'MISSING_PARAMETERS_ERROR'
  | 'MALFORMED_INPUT'
  | 'DRAGONCHAIN_CONTRACT_ERROR';

export class FailureByDesign extends Error {
  public readonly code: string;
  public readonly message: any;

  public constructor(code: ErrorCode, message: any) {
    super(message);
    this.code = code || 'FAILURE_BY_DESIGN';
    this.message = message || '';
  }
}
