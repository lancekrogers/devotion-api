import { Application } from './app';

const application = new Application();
application.startServer();
