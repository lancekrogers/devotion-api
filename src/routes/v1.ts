import express from 'express';
import * as actions from '../controller/actions';

export const v1Routes = (app: express.Application) => {
  const v1Router = express.Router();
  app.use('/v1', v1Router);

  v1Router.post('/register', actions.registerDevotion);
  v1Router.post('/cancel', actions.cancelDevotion);
  v1Router.get('/devotions/:quipuId', actions.getDevotions);
  v1Router.get('/sum/:quipuId', actions.getSum);
};
