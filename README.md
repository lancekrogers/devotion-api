# Devotion Service

We use typescript for our ECMAScript projects to ensure higher code quality and development conveniences.

## Source Code

All the typescript source code should be placed into the `src/` folder in the root of the repository.

## Ignore Files

The `.gitignore` and `.dockerignore` files from this folder should be copied over to your project repository, modifying for whatever is additionally necessary as needed.

## Linting/Formatting

The `"prettier"` and `"eslintConfig"` sections should be copied from the package.json file. These are the default settings for both tools.

Additionally, the `.prettierignore`, and `.eslintignore` files should all be copied into the repository, changing if necessary.

Checking linting can be done with `yarn lint`, and auto-formatting the whole project can be accomplished with `yarn format`.

## Typescript Config

Ensure that a `types.d.ts` file exists in the `src/` folder, and copy the `tsconfig.json` file into the root of the repository.

## Testing

We use nyc/mocha/chai/sinon for testing. Ensure the `"nyc"` section from the package.json file is copied over appropriately.

All unit tests should be supplied next to the source files, as `<source_file_name>.spec.ts`. These will be automatically found and run by running `yarn test`.

## Scripts

Useful scripts can be copies from the `"scripts"` section of the package.json as appropriate. Some of these may be necessary for docker to build.

## Dependencies

We use `yarn` for our dependency managment (NOT `npm`) because of its better ability to interact with lockfiles.

If all of the above was followed, some dependencies need to be added to the project with the following commands:

`yarn add tslib`

`yarn add --dev chai mocha nyc sinon eslint eslint-config-prettier prettier ts-node typescript @types/node @types/chai @types/mocha @types/sinon @typescript-eslint/eslint-plugin @typescript-eslint/parser`

Note that you MUST have a `yarn.lock` file for the Docker container to build. This file should definitely be committed!

Also copy over the `yarnclean` file to the root of the repository. This is use to clean the `node_modules` directory on docker build.

## Docker

The `Dockerfile` file should be copied to build the application. Modify if necessary.

## VSCode Config

VSCode can be configured to integrate with typescript/linting/formatting.

The following extensions should be installed:

[eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

[prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

Once these extensions are installed, the following settings should be set in vscode's `settings.json` (either for global, or just per workspace):

```json
{
  "editor.formatOnSave": true,
  "eslint.packageManager": "yarn",
  "eslint.alwaysShowStatus": true,
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "typescript",
    "typescriptreact"
  ],
  "prettier.eslintIntegration": true
}
```
