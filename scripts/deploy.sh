export VERSION=$(cat package.json | jq -r '.version')

aws --region us-west-2 cloudformation deploy --stack-name devotion-service-cicd --template-file ../cloudformation/cft.cicd.yml --capabilities CAPABILITY_NAMED_IAM
aws --region us-west-2 cloudformation deploy --stack-name devotion-service-config-staging --template-file ../cloudformation/cft.service.yml --capabilities CAPABILITY_NAMED_IAM
aws --region us-west-2 cloudformation deploy --stack-name devotion-service-config-production --parameter-overrides Stage=production --template-file ../cloudformation/cft.service.yml --capabilities CAPABILITY_NAMED_IAM
